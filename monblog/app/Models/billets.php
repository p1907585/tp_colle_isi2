<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class billets extends Model
{
    use HasFactory;

    //get the list of manga stored in the manga table.
    public function getAll()
    {
        #code 
        $billets = DB::table('billets')->get();
        return $billets;
    }
}
