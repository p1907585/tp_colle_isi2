<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorebilletsRequest;
use App\Http\Requests\UpdatebilletsRequest;
use App\Models\billets;

class BilletsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $billet = new billets();
        $billets = $billet->getAll();
        return view('index', compact('billets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorebilletsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorebilletsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\billets  $billets
     * @return \Illuminate\Http\Response
     */
    public function show(billets $billets)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\billets  $billets
     * @return \Illuminate\Http\Response
     */
    public function edit(billets $billets)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatebilletsRequest  $request
     * @param  \App\Models\billets  $billets
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatebilletsRequest $request, billets $billets)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\billets  $billets
     * @return \Illuminate\Http\Response
     */
    public function destroy(billets $billets)
    {
        //
    }
}
