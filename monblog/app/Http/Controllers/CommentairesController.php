<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorecommentairesRequest;
use App\Http\Requests\UpdatecommentairesRequest;
use App\Models\commentaires;

class CommentairesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorecommentairesRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorecommentairesRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\commentaires  $commentaires
     * @return \Illuminate\Http\Response
     */
    public function show(commentaires $commentaires)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\commentaires  $commentaires
     * @return \Illuminate\Http\Response
     */
    public function edit(commentaires $commentaires)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatecommentairesRequest  $request
     * @param  \App\Models\commentaires  $commentaires
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatecommentairesRequest $request, commentaires $commentaires)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\commentaires  $commentaires
     * @return \Illuminate\Http\Response
     */
    public function destroy(commentaires $commentaires)
    {
        //
    }
}
